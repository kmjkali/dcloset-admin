import customLoading from './modules/custom-loading'
import authenticated from './modules/authenticated'
import menu from './modules/menu'
import testData from './modules/test-data'
import goods from './modules/goods'
import order from './modules/order'
import delivery from './modules/delivery'
import member from './modules/member'
import notice from './modules/notice'
export const state = () => ({})

export const mutations = {}

export const modules = {
    customLoading,
    authenticated,
    menu,
    testData,
    goods,
    member,
    notice,
    delivery,
    order,
}
