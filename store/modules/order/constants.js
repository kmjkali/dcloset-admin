export default {
    DO_ORDER_LIST: 'order/doList',
    DO_ORDER_LIST_PAGING: 'order/doListPaging',
    DO_ORDER_DETAIL: 'order/doDetail',
    DO_ORDER_CREATE_COMMENT: 'order/doCreateComment',
    DO_ORDER_UPDATE: 'order/doUpdate',
    DO_ORDER_DELETE: 'order/doDelete',
    DO_ORDER_CREATE: 'order/doCreate'
}
