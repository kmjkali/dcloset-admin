import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_ORDER_LIST]: (store) => {
        return axios.get(apiUrls.DO_ORDER_LIST)
    },
    [Constants.DO_ORDER_LIST_PAGING]: (store, payload) => {
        let paramArray = []
        if (payload.params.searchName.length >= 1) paramArray.push(`searchName=${payload.params.searchName}`)
        if (payload.params.searchAge != null) paramArray.push(`searchAge=${payload.params.searchAge}`)
        if (payload.params.searchYear != null) paramArray.push(`searchYear=${payload.params.searchYear}`)
        if (payload.params.searchMonth != null) paramArray.push(`searchMonth=${payload.params.searchMonth}`)
        let paramText = paramArray.join('&')

        return axios.get(apiUrls.DO_ORDER_LIST_PAGING.replace('{pageNum}', payload.pageNum) + '?' + paramText)
    },
    [Constants.DO_ORDER_DETAIL]: (store, payload) => {
        return axios.get(apiUrls.DO_ORDER_DETAIL.replace('{id}', payload.id))
    },
    [Constants.DO_ORDER_CREATE_COMMENT]: (store, payload) => {
        return axios.post(apiUrls.DO_ORDER_CREATE_COMMENT.replace('{id}', payload.id), payload.data)
    },
    [Constants.DO_ORDER_UPDATE]: (store, payload) => {
        return axios.put(apiUrls.DO_ORDER_UPDATE.replace('{id}', payload.id), payload.data)
    },
    [Constants.DO_ORDER_DELETE]: (store, payload) => {
        return axios.delete(apiUrls.DO_ORDER_DELETE.replace('{id}', payload.id))
    },
    [Constants.DO_ORDER_CREATE]: (store, payload) => {
        return axios.post(apiUrls.DO_ORDER_CREATE, payload)
    },
}
