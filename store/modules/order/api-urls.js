const BASE_URL = 'http://d-closet.shop:8080/v1/order'

export default {
    DO_ORDER_LIST: `${BASE_URL}/all`, //get
    DO_ORDER_LIST_PAGING: `${BASE_URL}/page/{pageNum}`, //get
    DO_ORDER_DETAIL: `${BASE_URL}/{id}`, //get
    DO_ORDER_CREATE_COMMENT: `${BASE_URL}/comment/document-id/{id}`, //post
    DO_ORDER_UPDATE: `${BASE_URL}/{id}`, //put
    DO_ORDER_DELETE: `${BASE_URL}/{id}`, //del
    DO_ORDER_CREATE: `${BASE_URL}/new`, //post
}
