import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {//스크립트는 타입이 없으므로 무조건 페이로드로 존재한다.
    [Constants.DO_LOGIN]: (store, payload) => {
        return axios.post(apiUrls.DO_LOGIN, payload)
            .then((res) => {
                store.commit(Constants.FETCH_TOKEN, res.data.data.token) //res.data 전화건 결과물 .데이터.토큰
                store.commit(Constants.FETCH_NAME, res.data.data.name)
            })
    },
    [Constants.DO_LOGOUT]: (store) => {
        store.commit(Constants.REMOVE_TOKEN)
    },
    [Constants.DO_PASSWORD_CHANGE]: (store, payload) => {
        return axios.put(apiUrls.DO_PASSWORD_CHANGE, payload)
    },
    [Constants.DO_PROFILE]: (store) => {
        return axios.get(apiUrls.DO_PROFILE)
    }
}
