const BASE_URL = 'http://d-closet.shop:8080/v1/login'

export default {
    DO_LOGIN: `${BASE_URL}/member`, //post
    DO_PASSWORD_CHANGE: `${BASE_URL}/profile/password`, //put
    DO_PROFILE: `${BASE_URL}/profile/info`, //get
}
