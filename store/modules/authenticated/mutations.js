import Constants from './constants'
import state from './state'

export default {
    [Constants.REMOVE_TOKEN]: (state) => {
        window.$nuxt.$cookies.remove('token')
    },

    [Constants.FETCH_TOKEN]: (state, payload) => {
        window.$nuxt.$cookies.set('token', payload, {
            path: '/',
            maxAge: 36000 //쿠키의 나이 , 36000초
        })
    },
    [Constants.FETCH_NAME]: (state, payload) => {
        state.name = payload
    },
}
