const state = () => ({
    globalMenu: [
        {
            parentName: 'HOME',
            menuLabel: [
                { id: 'DASH_BOARD', icon: 'el-icon-menu', currentName: '대시보드', link: '/main', isShow: true},
                { id: 'MEMBER_LIST', icon: 'el-icon-user', currentName: '고객 관리', link: '/member/list', isShow: true },
                { id: 'PAYMENT_LIST', icon: 'el-icon-bank-card', currentName: '결제 관리', link: '/payment/list', isShow: true },
                { id: 'ORDER_LIST', icon: 'el-icon-document', currentName: '주문 관리', link: '/order/list', isShow: true },
                { id: 'DELIVERY_LIST', icon: 'el-icon-truck', currentName: '배송 관리', link: '/delivery/list', isShow: true },
                { id: 'GOODS_LIST', icon: 'el-icon-suitcase', currentName: '상품 관리', link: '/goods/list', isShow: true },
                { id: 'MAGAZINE_LIST', icon: 'el-icon-film', currentName: '매거진 관리', link: '/magazine/list', isShow: true },
                {
                    id: 'BOARD',
                    icon: 'el-icon-chat-dot-square',
                    currentName: '게시판 관리',
                    link: '',
                    isShow: true,
                    subMenu: [
                        { id: 'NOTICE_LIST', icon: 'el-icon-chat-line-square', currentName: '공지사항 관리', link: '/notice/list', isShow: true, parentName: '게시판', },
                        { id: 'CS_LIST', icon: 'el-icon-chat-dot-square', currentName: 'CS 관리', link: '/cs/list', isShow: true, parentName: '게시판' },
                        { id: 'EVENT_LIST', icon: 'el-icon-magic-stick', currentName: '이벤트게시판 관리', link: '/event/list', isShow: true, parentName: '게시판' },

                    ],
                },
            ]
        },



    ],
    selectedMenu: 'DASH_BOARD'

})

export default state
