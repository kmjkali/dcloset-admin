import Constants from './constants'
import state from './state'

//@TODO 브레드 메뉴 3번으로 추가할것

export default {
    [Constants.GET_MENUS_LIST]: (state) => {
        return state.globalMenu
    },
    [Constants.GET_SELECTED_MENU]: (state) => {
        let result = []
        state.globalMenu.forEach(function (element1) {
            // 첫번째 (Home)
            let tempParentName = element1.parentName
            element1.menuLabel.forEach(function (element2) {
                // 두번째에서 서브메뉴가 없으면
                if ( !element2.subMenu ){
                    if (element2.id === state.selectedMenu){
                        // 두번째
                        result.push(tempParentName)
                        result.push(element2.currentName)
                    }
                } else {
                    // 서브메뉴 있으면 세번째까지 표시
                    element2.subMenu.forEach((subMenu)=>{
                        if ( subMenu.id == state.selectedMenu ){
                            // 세번째
                            result.push(tempParentName)
                            result.push(element2.currentName)
                            result.push(subMenu.currentName)
                        }
                    })
                }

            })
        })
        console.log('완료 됬구요 브레드에 찍을데이튼요', result);
        return result
    }
}
