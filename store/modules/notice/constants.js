export default {
    //@TODO 공지사항 리스트 조회
    DO_NOTICE_LIST: '/notice/list',
    //@TODO 공지사항 리스트 페이징
    DO_NOTICE_LIST_PAGING: '/notice/ListPaging',
    //@TODO 공지사항 단수 조회
    DO_NOTICE_DETAIL: '/notice/detail/{id}',
    //@TODO 공지사항 등록 코멘트
    DO_CREATE_NOTICE_COMMENT: '/notice/addComment',
    //@TODO 공지사항 수정
    DO_NOTICE_UPDATE: '/notice/update',
    //@TODO 공지사항 삭제
    DO_NOTICE_DELETE: '/notice/delete',
    //@TODO 공지사항 글쓰기
    DO_NOTICE_CREATE: '/notice/detail/add'
}
