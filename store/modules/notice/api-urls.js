const BASE_URL = 'http://d-closet.shop:8080/v1/NoticeBulletin'

export default {
    DO_NOTICE_LIST: `${BASE_URL}/list/all`, //get
    DO_NOTICE_LIST_PAGING: `${BASE_URL}/page/{pageNum}`, //get
    DO_NOTICE_DETAIL: `${BASE_URL}/detail/{id}`, //get
    DO_NOTICE_CREATE_COMMENT: `${BASE_URL}/comment/document-id/{id}`, //post
    DO_NOTICE_UPDATE: `${BASE_URL}/update/{id}`, //put
    DO_NOTICE_DELETE: `${BASE_URL}/delete/{id}`, //del
    DO_NOTICE_CREATE: `${BASE_URL}/new/member-id/{memberId}`, //post
}
