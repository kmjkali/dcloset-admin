export default {
    DO_DELIVERLY_LIST: 'delivery/doList',
    DO_DELIVERLYLIST_PAGING: 'delivery/doListPaging',
    DO_DELIVERLYDETAIL: 'delivery/doDetail',
    DO_DELIVERLYCREATE_COMMENT: 'delivery/doCreateComment',
    DO_DELIVERLYUPDATE: 'delivery/doUpdate',
    DO_DELIVERLYDELETE: 'delivery/doDelete',
    DO_DELIVERLYCREATE: 'delivery/doCreate'
}
