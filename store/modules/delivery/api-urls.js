const BASE_URL = 'http://d-closet.shop:8080/v1/delivery'

export default {
    DO_DELIVERLY_LIST: `${BASE_URL}/all`, //get
    DO_DELIVERLY_LIST_PAGING: `${BASE_URL}/page/{pageNum}`, //get
    DO_DELIVERLY_DETAIL: `${BASE_URL}/{id}`, //get
    DO_DELIVERLY_CREATE_COMMENT: `${BASE_URL}/comment/document-id/{id}`, //post
    DO_DELIVERLY_UPDATE: `${BASE_URL}/{id}`, //put
    DO_DELIVERLY_DELETE: `${BASE_URL}/{id}`, //del
    DO_DELIVERLY_CREATE: `${BASE_URL}/new`, //post
}
