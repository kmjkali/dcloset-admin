// @TODO 도메인 추가할 것
const BASE_URL = `http://d-closet.shop:8080/v1/goods`

export default {
    // 상품목록 조회 API URL 연결 GET
    DO_GOODS_LIST: `${BASE_URL}/all`,
    // 상품상세 조회 API URL 연결 GET
    DO_GOODS_DETAIL: `${BASE_URL}/detail/id/{id}`,
    // 상품목록 등록 API URL POST
    DO_GOODS_ADD: `${BASE_URL}/new`,

}
