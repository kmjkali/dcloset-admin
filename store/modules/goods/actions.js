import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'
import {store} from "core-js/internals/reflect-metadata";
import payment from "@/pages/payment/list.vue";

export default {
    [Constants.DO_GOODS_LIST]: (store) => {
        return axios.get(apiUrls.DO_GOODS_LIST)
    },

    // payload : 파라미터( 여기서는 등록할 상품정보 )
    [Constants.DO_GOODS_ADD]: (store, payload) => {
        return axios.post(apiUrls.DO_GOODS_ADD,payload)
    },

    // 상품 상세 조회
    [Constants.DO_GOODS_DETAIL]:(store, payload) =>{
        return axios.get(apiUrls.DO_GOODS_DETAIL.replace('{id}', payload.id))
    }

}
