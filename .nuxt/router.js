import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _6a252a0f = () => interopDefault(import('../pages/main.vue' /* webpackChunkName: "pages/main" */))
const _264b5468 = () => interopDefault(import('../pages/test.vue' /* webpackChunkName: "pages/test" */))
const _e9e07022 = () => interopDefault(import('../pages/customer/form.vue' /* webpackChunkName: "pages/customer/form" */))
const _81ee226e = () => interopDefault(import('../pages/customer/list.vue' /* webpackChunkName: "pages/customer/list" */))
const _71c28906 = () => interopDefault(import('../pages/customer/list-filter-paging.vue' /* webpackChunkName: "pages/customer/list-filter-paging" */))
const _ba702048 = () => interopDefault(import('../pages/delivery/add.vue' /* webpackChunkName: "pages/delivery/add" */))
const _52b48b73 = () => interopDefault(import('../pages/delivery/list.vue' /* webpackChunkName: "pages/delivery/list" */))
const _612ea8c6 = () => interopDefault(import('../pages/event/add.vue' /* webpackChunkName: "pages/event/add" */))
const _6124efc9 = () => interopDefault(import('../pages/event/list.vue' /* webpackChunkName: "pages/event/list" */))
const _8bb55cfc = () => interopDefault(import('../pages/goods/add.vue' /* webpackChunkName: "pages/goods/add" */))
const _27035e8d = () => interopDefault(import('../pages/goods/list.vue' /* webpackChunkName: "pages/goods/list" */))
const _41b18fd3 = () => interopDefault(import('../pages/magazine/list.vue' /* webpackChunkName: "pages/magazine/list" */))
const _96e79fe6 = () => interopDefault(import('../pages/member/list.vue' /* webpackChunkName: "pages/member/list" */))
const _39f43722 = () => interopDefault(import('../pages/my-menu/logout.vue' /* webpackChunkName: "pages/my-menu/logout" */))
const _3aef8380 = () => interopDefault(import('../pages/notice/add.vue' /* webpackChunkName: "pages/notice/add" */))
const _0afe098f = () => interopDefault(import('../pages/notice/list.vue' /* webpackChunkName: "pages/notice/list" */))
const _4d73e595 = () => interopDefault(import('../pages/order/list.vue' /* webpackChunkName: "pages/order/list" */))
const _48dcd25d = () => interopDefault(import('../pages/payment/list.vue' /* webpackChunkName: "pages/payment/list" */))
const _33f1887e = () => interopDefault(import('../pages/question/list.vue' /* webpackChunkName: "pages/question/list" */))
const _5245ae50 = () => interopDefault(import('../pages/magazine/detail/add.vue' /* webpackChunkName: "pages/magazine/detail/add" */))
const _96a28832 = () => interopDefault(import('../pages/customer/detail/_id.vue' /* webpackChunkName: "pages/customer/detail/_id" */))
const _09814160 = () => interopDefault(import('../pages/customer/edit/_id.vue' /* webpackChunkName: "pages/customer/edit/_id" */))
const _0918b111 = () => interopDefault(import('../pages/delivery/detail/_id.vue' /* webpackChunkName: "pages/delivery/detail/_id" */))
const _3805fce7 = () => interopDefault(import('../pages/event/detail/_id.vue' /* webpackChunkName: "pages/event/detail/_id" */))
const _0d1f48ab = () => interopDefault(import('../pages/goods/detail/_id.vue' /* webpackChunkName: "pages/goods/detail/_id" */))
const _7598ed71 = () => interopDefault(import('../pages/magazine/detail/_id.vue' /* webpackChunkName: "pages/magazine/detail/_id" */))
const _83b60baa = () => interopDefault(import('../pages/member/detail/_id.vue' /* webpackChunkName: "pages/member/detail/_id" */))
const _7e0baba6 = () => interopDefault(import('../pages/notice/detail/_id.vue' /* webpackChunkName: "pages/notice/detail/_id" */))
const _84652242 = () => interopDefault(import('../pages/question/detail/_id.vue' /* webpackChunkName: "pages/question/detail/_id" */))
const _88161828 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/main",
    component: _6a252a0f,
    name: "main"
  }, {
    path: "/test",
    component: _264b5468,
    name: "test"
  }, {
    path: "/customer/form",
    component: _e9e07022,
    name: "customer-form"
  }, {
    path: "/customer/list",
    component: _81ee226e,
    name: "customer-list"
  }, {
    path: "/customer/list-filter-paging",
    component: _71c28906,
    name: "customer-list-filter-paging"
  }, {
    path: "/delivery/add",
    component: _ba702048,
    name: "delivery-add"
  }, {
    path: "/delivery/list",
    component: _52b48b73,
    name: "delivery-list"
  }, {
    path: "/event/add",
    component: _612ea8c6,
    name: "event-add"
  }, {
    path: "/event/list",
    component: _6124efc9,
    name: "event-list"
  }, {
    path: "/goods/add",
    component: _8bb55cfc,
    name: "goods-add"
  }, {
    path: "/goods/list",
    component: _27035e8d,
    name: "goods-list"
  }, {
    path: "/magazine/list",
    component: _41b18fd3,
    name: "magazine-list"
  }, {
    path: "/member/list",
    component: _96e79fe6,
    name: "member-list"
  }, {
    path: "/my-menu/logout",
    component: _39f43722,
    name: "my-menu-logout"
  }, {
    path: "/notice/add",
    component: _3aef8380,
    name: "notice-add"
  }, {
    path: "/notice/list",
    component: _0afe098f,
    name: "notice-list"
  }, {
    path: "/order/list",
    component: _4d73e595,
    name: "order-list"
  }, {
    path: "/payment/list",
    component: _48dcd25d,
    name: "payment-list"
  }, {
    path: "/question/list",
    component: _33f1887e,
    name: "question-list"
  }, {
    path: "/magazine/detail/add",
    component: _5245ae50,
    name: "magazine-detail-add"
  }, {
    path: "/customer/detail/:id?",
    component: _96a28832,
    name: "customer-detail-id"
  }, {
    path: "/customer/edit/:id?",
    component: _09814160,
    name: "customer-edit-id"
  }, {
    path: "/delivery/detail/:id?",
    component: _0918b111,
    name: "delivery-detail-id"
  }, {
    path: "/event/detail/:id?",
    component: _3805fce7,
    name: "event-detail-id"
  }, {
    path: "/goods/detail/:id?",
    component: _0d1f48ab,
    name: "goods-detail-id"
  }, {
    path: "/magazine/detail/:id?",
    component: _7598ed71,
    name: "magazine-detail-id"
  }, {
    path: "/member/detail/:id?",
    component: _83b60baa,
    name: "member-detail-id"
  }, {
    path: "/notice/detail/:id?",
    component: _7e0baba6,
    name: "notice-detail-id"
  }, {
    path: "/question/detail/:id?",
    component: _84652242,
    name: "question-detail-id"
  }, {
    path: "/",
    component: _88161828,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
