import bindStorage from 'nuxt-vuex-localstorage/plugins/bindStorage'

export default (ctx) => {
  const options = {"localStorage":["authenticated"]}
  bindStorage(ctx, options)
}
