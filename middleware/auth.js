export default (context) => {
    if (!context.app.context.app.$cookies.get('token')) {//토큰값이 없으면
        return context.redirect('/login') //페이지를 강제로 로그인페이지로 보내겠다
    }
}
